from django.conf.urls import url
from .views import ValidateView,ConfirmView,SubmitView

urlpatterns = [
    url(r'^validate/', ValidateView.as_view(), name='validate'),
    url(r'^confirm/', ConfirmView.as_view(), name='confirm'),
    url(r'^submit/', SubmitView.as_view(), name='confirm'),
]