import requests
from requests.auth import HTTPBasicAuth
from M2Crypto import RSA, X509
import base64 
import json
import datetime

INITIATOR_PASS  = "6ZqK7mRa"
CERTIFICATE_FILE = "cert.cer"

consumer_key = "xKfIPt144qAp2SkK9p0Q4g1b5QVpLRAN"
consumer_secret = "2CNJGtUoeqN8n3Rr"

def encryptInitiatorPassword():
    cert_file = open(CERTIFICATE_FILE, 'r')
    cert_data = cert_file.read() #read certificate file
    cert_file.close()

    cert = X509.load_cert_string(cert_data)
    #pub_key = X509.load_cert_string(cert_data)
    pub_key = cert.get_pubkey()
    rsa_key = pub_key.get_rsa()
    cipher = rsa_key.public_encrypt(INITIATOR_PASS, RSA.pkcs1_padding)
    return b64encode(cipher)

    print encryptInitiatorPassword()

def get_token():
    api_URL = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"

    consumer_key = "xKfIPt144qAp2SkK9p0Q4g1b5QVpLRAN"
    consumer_secret = "2CNJGtUoeqN8n3Rr"

    r = requests.get(api_URL, auth=HTTPBasicAuth(consumer_key, consumer_secret))
    jonresponse = json.loads(r.content)
    access_token = jonresponse['access_token']
    print(access_token)
    return access_token

def register_url(access_token):
    api_url = "http://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl"
    headers = {"Authorization": "Bearer %s" % access_token}
    request = { "ShortCode": "600742",
        "ResponseType": " ",
        "ConfirmationURL": "http://ec5be0f8.ngrok.io/api/confirm/",
        "ValidationURL": "http://ec5be0f8.ngrok.io/api/validate/"}

    response = requests.post(api_url, json = request, headers=headers)

    print (response.text)


def sendSTK(phone_number, amount):
    access_token = get_token()
    print('starting')
    time_now = datetime.datetime.now().strftime("%Y%m%d%H%I%S")

    PASS_KEY  = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"
    shortcode = "174379"
    #now = datetime.datetime.now().strftime("%Y%m%d%H%I%S")
    s = shortcode + PASS_KEY + time_now
    encoded = base64.b64encode(s.encode('utf-8'))

    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
    headers = { "Authorization": "Bearer %s" % access_token }
    request = {
    "BusinessShortCode": "174379",
    "Password": encoded,
    "Timestamp": time_now,
    "TransactionType": "CustomerPayBillOnline",
    "Amount": amount,
    "PartyA": phone_number,
    "PartyB": "174379",
    "PhoneNumber": phone_number,
    "CallBackURL": "http://c23aabc2.ngrok.io/api/confirm/",
    "AccountReference": "600000",
    "TransactionDesc": "6007422"
    }

    response = requests.post(api_url, json = request, headers=headers)

    print (response.text)
