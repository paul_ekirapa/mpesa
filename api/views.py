# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.views import APIView
import json
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
import json
from .c2b import sendSTK
# Create your views here.

class SubmitView(APIView):
    def post(self,request):
        data = request.data
        phone_number = data['phone_number']
        amount = data['amount']

        print(phone_number)
        print(amount)
        sendSTK(phone_number,amount)
        message = {"status":"ok"}
        return Response(message, status=HTTP_200_OK)
    

class ConfirmView(APIView):
    def post(self, request):
        #save the data
        request_data = request.data

        #Perform your processing here e.g. print it out...
        print(request_data)

        # Prepare the response, assuming no errors have occurred. Any response
        # other than a 0 (zero) for the 'ResultCode' during Validation only means
        # an error occurred and the transaction is cancelled
        message = {
            "ResultCode": 0,
            "ResultDesc": "The service was accepted successfully",
            "ThirdPartyTransID": "1237867865"
        };

        # Send the response back to the server
        return Response(message, status=HTTP_200_OK)

class ValidateView(APIView):
    def post(self, request):
        #save the data
        request_data = request.data

        #Perform your processing here e.g. print it out...
        print(request_data)

        # Prepare the response, assuming no errors have occurred. Any response
        # other than a 0 (zero) for the 'ResultCode' during Validation only means
        # an error occurred and the transaction is cancelled
        message = {
            "ResultCode": 0,
            "ResultDesc": "The service was accepted successfully",
            "ThirdPartyTransID": "1234567890"
        };

        # Send the response back to the server
        return Response(message, status=HTTP_200_OK)
